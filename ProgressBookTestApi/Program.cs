﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace ProgressBookTestApi
{
    class Program
    {
        private static HttpClient HttpClient;
        static async Task Main(string[] args)
        {
            var subscriptionKey = args.Length > 0 ? args[0] : null;
            if (subscriptionKey == null)
            {
                Console.WriteLine();
                Console.Write("Enter your Ocp-Apim-Subscription-Key: ");
                subscriptionKey = Console.ReadLine();
            }

            var token = args.Length > 1 ? args[1] : null;
            if (token == null)
            {
                Console.WriteLine();
                Console.Write("Enter a valid Bearer Token: ");
                token = Console.ReadLine();
            }

            HttpClient = new HttpClient();
            HttpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);
            HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            Console.WriteLine();
            Console.WriteLine("Getting district info...");
            var districtId = (await CallApi("/SisService/District"))["result"][0]["RefId"].Value<string>();
            Console.WriteLine($"District ID is: {districtId}");

            Console.WriteLine("Getting school info...");
            var schoolInfo = (await CallApi($"/SisService/SchoolInfo?leaOrSchoolInfoRefId={districtId}&page=1&pageSize=100"))["result"] as JArray;
            var schools = schoolInfo.Select(s => new { RefId = s["RefId"].Value<string>(), Name = s["SchoolName"].Value<string>() }).ToList();

            Console.WriteLine("");
            for (var i = 0; i < schools.Count; i++)
            {
                Console.Write(i + 1);
                Console.WriteLine(" " + schools[i].Name);
            }

            Console.Write("Select a school: ");
            var schoolId = schools[int.Parse(Console.ReadLine()) - 1].RefId;
            Console.WriteLine("Grabbing the first 1000 student records");
            var studentIds = (await CallApi(
                    $"/SisService/StudentSnapshot_v2?schoolId={schoolId}&page=1&pageSize=1000"))["result"]
                .Value<JArray>()
                .Select(s => s["StudentPersonalId"].Value<string>()).ToList();

            Console.WriteLine("Grabbing photos");
            var maxTasks = 10;
            var photoTasks = new List<Task<JObject>>();
            var successTaskCount = 0;
            var failTaskCount = 0;
            var totalPhotoCount = 0;
            foreach (var studentId in studentIds)
            {
                photoTasks.Add(CallApi($"/SisService/StudentPicture?studentPersonalRefId={studentId}&page=1&pageSize=1"));
                if (photoTasks.Count >= maxTasks)
                {
                    try
                    {
                        await Task.WhenAny(photoTasks);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Exception thrown... {e.Message}");
                    }
                }

                totalPhotoCount += photoTasks.Count(t => t.IsCompletedSuccessfully && t.Result != null && t.Result["count"].Value<int>() > 0);
                successTaskCount += photoTasks.RemoveAll(t => t.IsCompletedSuccessfully);
                failTaskCount += photoTasks.RemoveAll(t => t.IsCompleted);
            }

            if (photoTasks.Count > 0)
            {
                try
                {
                    await Task.WhenAll(photoTasks);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Exception thrown... {e.Message}");
                }

                totalPhotoCount += photoTasks.Count(t => t.IsCompletedSuccessfully && t.Result != null && t.Result["count"].Value<int>() > 0);
                successTaskCount += photoTasks.RemoveAll(t => t.IsCompletedSuccessfully);
                failTaskCount += photoTasks.RemoveAll(t => t.IsCompleted);
            }

            Console.WriteLine($"Total photos returned: {totalPhotoCount}");
            Console.WriteLine($"Finished! Request success: {successTaskCount}, fail: {failTaskCount}.");
        }

        private static async Task<JObject> CallApi(string path)
        {
            var timeout = TimeSpan.FromSeconds(30); 
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"https://pbapi.azure-api.net/VendorLink-Test/{path.TrimStart('/')}")
            };
            try
            {
                var cancellationToken = new CancellationTokenSource(timeout);
                using var response = await HttpClient.SendAsync(requestMessage, cancellationToken.Token);
                await using var stream = await response.Content.ReadAsStreamAsync();
                using var sr = new StreamReader(stream);

                return JObject.Parse(await sr.ReadToEndAsync());
            }
            catch (Exception e)
            {
                Console.WriteLine($"An exception was thrown for url {requestMessage.RequestUri}");
                throw;
            }
            finally
            {
                requestMessage.Dispose();
            }
        }
    }
}
