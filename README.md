# ProgressBook API Testing #


### How do I run this? ###

* Grab a Ocp-Apim-Subscription-Key and a valid Bearer Token
* Start the app
* Answer any questions. For testing, I was using "VendorLink High School".

### Then what? ###

You should see a list of URLs that failed. It may run fine the first or second time through, but keep trying. Try running one of those failed
urls in another app (maybe Postman?) and you can see the returned `500`.

